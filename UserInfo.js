class UserInfo {
  constructor(elem, api, errHandler) {
    this.elem = elem;
    this.api = api;
    this.errHandler = errHandler;
    this.id = null;
  }

  getInfo() {
    console.log('Запрос данных о пользователе');
    this.api
      .getUserInfo()
      .then(data => {
        const { name, avatar, _id } = data;

        console.log(name, avatar, _id);
        this.id = _id; // нужен для иконок удаление (по нему будем определять свои карточки)

        this.setInfo(name, avatar);
      })
      .catch(err => {
        this.errHandler.showError(err);
      });
  }

  setInfo(name, avatar) {
    this.elem.innerHTML = `
      <img src="${avatar}" class="user-info__avatar">
      <div class="user-info__name">${name}</div>
    `;
  }

  getMyId() {
    return this.id;
  }
}

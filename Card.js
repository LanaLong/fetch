class Card {
  constructor(name, link, id, isMine, api) {
    this.name = name;
    this.link = link;
    this.id = id;
    this.elem = null;
    this.isMine = isMine;
    this.api = api;
  }

  // remove() {
  //   // варианты
  //   //   1: удалить карточку здесь
  //   //   2: удалить её в родителе (больше нравится, т.к. в cardList уже есть api и errHandler)
  //   console.log('delete card pressed');
  // }

  create() {
    this.elem = document.createElement('div');
    this.elem.classList.add('card');
    this.elem.dataset.id = this.id;

    const template = `
      <h3 class="card__name">${this.name}</h3>
      ${this.id}
      ${this.isMine ? '<button class="card__button">X</button>' : ''}
    `;

    this.elem.innerHTML = template;

    const deleteButton = this.elem.querySelector('.card__button');

    if (deleteButton) {
      deleteButton.addEventListener('click', this.remove.bind(this));
    }

    return this.elem;
  }

  remove() {
    this.api
      .deleteCard(this.id)
      .then(res => {
        console.log(res);
        this.elem.remove();
      })
      .catch(err => {
        console.log(err);
        // this.errHandler.showError(err);
      });
  }
}

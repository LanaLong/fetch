class Api {
  constructor(groupId, token) {
    this.baseUrl = `https://praktikum.tk/${groupId}`;
    this.token = token;
  }

  getUserInfo() {
    return fetch(`${this.baseUrl}/users/me`, {
      headers: {
        authorization: this.token
      }
    })
      .then(res => this.parseResponce(res))
      .catch(err => {
        // console.log(err);
        throw err; // выбрасываем исключение - генерация ошибки пользователем -> поймаем её на уровень выше и там сможем отобразить в интерфейсе
      });
  }

  loadCards() {
    return fetch(`${this.baseUrl}/cards`, {
      headers: {
        authorization: this.token
      }
    })
      .then(res => this.parseResponce(res))
      .catch(err => {
        throw err;
      });
  }

  parseResponce(res) {
    if (res.ok) {
      return res.json();
    }

    return Promise.reject(`Что-то пошло не так: ${res.status}`);
  }

  deleteCard(id) {
    return fetch(`${this.baseUrl}/cards/${id}`, {
      method: 'DELETE',
      headers: {
        authorization: this.token
      }
    })
      .then(res => this.parseResponce(res))
      .catch(err => {
        throw err;
      });
  }
}

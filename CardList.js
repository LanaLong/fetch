class CardList {
  constructor(elem, makeCard, api, errHandler, userInfo) {
    this.elem = elem;
    this.makeCard = makeCard;
    this.api = api;
    this.errHandler = errHandler;
    this.userInfo = userInfo;
  }

  addCard(card) {
    const cardElem = card.create();
    this.elem.appendChild(cardElem);
  }

  render() {
    // показываем загрузку
    this.elem.innerHTML = 'Loading...';

    // получаем свой ID
    const myId = this.userInfo.getMyId();

    // добавляет обработчик кликов
    // this.setRemoveHandler();

    this.api
      .loadCards()
      .then(cards => {
        this.elem.innerHTML = '';
        console.log(cards);

        cards.forEach(data => {
          const isMine = data.owner._id === myId;

          const card = this.makeCard(data.name, data.link, data._id, isMine);
          this.addCard(card);
        });
      })
      .catch(err => {
        this.errHandler.showError(err);
      });
  }

  // setRemoveHandler() {
  //   this.elem.addEventListener('click', this.removeCard.bind(this));
  // }

  // removeCard(event) {
  //   if (!event.target.classList.contains('card__button')) {
  //     return;
  //   }

  //   const cardElem = event.target.closest('.card');
  //   const id = cardElem.dataset.id;

  //   console.log('remove card, id:', id);

  //   this.api
  //     .deleteCard(id)
  //     .then(res => {
  //       console.log(res);

  //       const card = this.elem.querySelector(`[data-id="${id}"]`); // DOM-элемент
  //       card.remove();
  //     })
  //     .catch(err => {
  //       this.errHandler.showError(err);
  //     });
  // }
}

// Elements
const getUserButton = document.querySelector('#get-user-info-btn');
const userInfoElem = document.querySelector('.user-info');

const errorElem = document.querySelector('.error-text');

const loadCardsButton = document.querySelector('#get-cards-btn');
const cardListElem = document.querySelector('.card-list');

const addCardForm = document.querySelector('.add-card-form');

// init instances
const errHandler = new ErrorHandler(errorElem);

const api = new Api('cohort9', 'ваш токен');

const userInfo = new UserInfo(userInfoElem, api, errHandler);

// callbacks
const createCard = (name, link, id, isMine) =>
  new Card(name, link, id, isMine, api);

const cardList = new CardList(
  cardListElem,
  createCard,
  api,
  errHandler,
  userInfo
);

// listeners
getUserButton.addEventListener('click', userInfo.getInfo.bind(userInfo));
loadCardsButton.addEventListener('click', cardList.render.bind(cardList));
